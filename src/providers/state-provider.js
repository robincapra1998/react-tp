import React, { createContext, useReducer } from 'react';

const initialState = {
  resultSearch: [],
  data: [{
    id: 1,
    title: 'Panda',
    text: 'big smile',
    img: 'https://media.istockphoto.com/photos/meme-like-portrait-of-laughing-middle-aged-man-humor-and-fun-concept-picture-id1330854667?b=1&k=20&m=1330854667&s=170667a&w=0&h=h-DYIkrnbP_ajDg7drCEt7gEmP5qKkqlAXZA3d1jR6o='
  }, {
    id: 2,
    title: 'big smile',
    text: 'Im the description 2',
    img: 'https://media.istockphoto.com/photos/meme-like-portrait-of-laughing-middle-aged-man-humor-and-fun-concept-picture-id1330854667?b=1&k=20&m=1330854667&s=170667a&w=0&h=h-DYIkrnbP_ajDg7drCEt7gEmP5qKkqlAXZA3d1jR6o='
  }]
};
const store = createContext(initialState);
const { Provider } = store;

const actionTypes = {
  ADD_SEARCH_RESULT: 'ADD_SEARCH_RESULT'
};

const StateProvider = ({ children }) => {
  const [state, dispatch] = useReducer((state, action) => {
    switch (action.type) {
      case actionTypes.SEARCH_RESULT: {
        return { ...state, resultSearch: action.payload };
      }
      default:
        return state;
    }
  }, initialState);
  return <Provider value={{ state, dispatch }}>{children}</Provider>;
};

export { store, StateProvider, actionTypes };

import React, { useContext } from 'react';
import { Link } from 'react-router-dom';
import {
  Button,
  Container,
  Form,
  FormControl,
  Nav,
  Navbar
} from 'react-bootstrap';
import { store } from '../providers/state-provider';



const Search = () => {

  const { state } = useContext(store);
  console.log(state.data)
  return (
    <Navbar bg="dark" expand="lg">
      <Container fluid>
        <Navbar.Brand>Borin La street</Navbar.Brand>
        <Navbar.Toggle aria-controls="navbarScroll" />
        <Navbar.Collapse id="navbarScroll">
          <Nav
            className="me-auto my-2 my-lg-0"
            style={{ maxHeight: '100px' }}
            navbarScroll
          >
            <Nav.Link>
              <Link to="/">Home</Link>
            </Nav.Link>
          </Nav>
          <Form className="d-flex">
            <FormControl
              type="search"
              placeholder="Search"
              className="me-2"
              aria-label="Search"
            />

            <Button variant="outline-success">Search</Button>
            <p></p>
            <p></p>
            {state.data.map((item) => {
              return (
              <>
                <p>{item.title}</p>
                <p>{item.text}</p>
              </>
              )
            }     
            )}
          </Form>
        </Navbar.Collapse>
      </Container>
    </Navbar>)
};

export default Search;

import React from 'react';

import {
  Col,
  Container,
  Row
} from 'react-bootstrap';

const Insight = () => (
  <Container>
    <Row>
      <Col>
        <h2> Item </h2>
      </Col>
    </Row>
    <Row>
      <Col sm={3}>
        <img width='100%' src='https://media.istockphoto.com/photos/meme-like-portrait-of-laughing-middle-aged-man-humor-and-fun-concept-picture-id1330854667?b=1&k=20&m=1330854667&s=170667a&w=0&h=h-DYIkrnbP_ajDg7drCEt7gEmP5qKkqlAXZA3d1jR6o=' alt='description'></img>
      </Col>
      <Col sm={9}>
        <h2> Description </h2>
        <span> I'm a small description</span>
        <ul>
          <li>I</li>
          <li> Am </li>
          <li> a </li>
          <li> list </li>
        </ul>
      </Col>
    </Row>
  </Container>
);

export default Insight;

import React, { useContext } from 'react';
import { Link } from 'react-router-dom';
import {
  Button,
  Col,
  Container,
  Card,
  Row
} from 'react-bootstrap';

import { store } from '../providers/state-provider';

const Item = ({ item }) => {
  const {
    id,
    title,
    text,
    img
  } = item;
  return (
    <Col sm={3} className="mt-3">
      <Card>
        <Card.Img variant="top" src={img} />
        <Card.Body>
          <Card.Title>{title}</Card.Title>
          <Card.Text>{text}</Card.Text>
        </Card.Body>
        <Card.Footer className="d-grid gap-2">
          <Button>
            <Link to='/info'>show { id }</Link>
          </Button>
        </Card.Footer>
      </Card>
    </Col>
  );
};

const SearchResult = () => {
  const { state } = useContext(store);

  return (
    <Container fluid>
      <Row>
        {state.data.map((item) => <Item key={item.id} item={item} />)}
      </Row>
    </Container>
  );
};

export default SearchResult;

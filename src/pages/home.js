import React from 'react';

import Search from '../components/search';
import SearchResult from '../components/search-result';
import { StateProvider } from '../providers/state-provider';

const Home = () => (
  <>
  <StateProvider>
    <Search/>
    <SearchResult/>
  </StateProvider>
  </>
);

export default Home;

import React from 'react';

import Search from '../components/search';
import Insight from '../components/insight';

const Info = () => (
  <>
    <Search/>
    <Insight/>
  </>
);

export default Info;

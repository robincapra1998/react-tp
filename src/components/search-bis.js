import React, { useContext, useRef } from 'react';
import { Link } from 'react-router-dom';
import {
  Button,
  Container,
  Form,
  FormControl,
  Nav,
  Navbar
} from 'react-bootstrap';

import { store, actionTypes } from '../providers/state-provider';

const Search = () => {
const { state, dispatch } = useContext(store);
const inputRef = useRef(null);

const getSearchByTitle = (value) => {
  const { data } = state;

  return data.filters((movie) => (movie.title === value));
}

return (
  <Navbar bg="dark" expand="lg">
    <Container fluid>
      <Navbar.Brand>Panda</Navbar.Brand>
      <Navbar.Toggle aria-controls="navbarScroll" />
      <Navbar.Collapse id="navbarScroll">
        <Nav
          className="me-auto my-2 my-lg-0"
          style={{ maxHeight: '100px' }}
          navbarScroll
        >
        <Nav.Link>
          <Link to="/">Home</Link>
        </Nav.Link>
        </Nav>
        <Form className="d-flex">
          <FormControl ref={inputref}
            type="search"
            placeholder="Search"
            className="me-2"
            aria-label="Search"
          />
          <Button variant="outline-success">Search</Button>
        </Form>
      </Navbar.Collapse>
    </Container>
  </Navbar>
)};

export default Search;
